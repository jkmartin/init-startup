#!/usr/bin/env sh

set -ox errexit
if [[ -z "${POD_NAME}" ]]; then
    echo "Environment variable POD_NAME not found..."
    env
    exit
fi

pod_name=${POD_NAME}
id=${pod_name##*-};
component_name=${POD_NAME%-*}

if [ -e "/mnt/config/pod/${component_name}/template.jsonnet" ]; then
  cat < /mnt/config/pod/"${component_name}"/template.jsonnet | base64 -d > /opt/template.jsonnet
  /usr/local/bin/jsonnet /opt/template.jsonnet --ext-str id="${id}" -o /opt/"${component_name}".json
  cat /opt/"${component_name}".json
  /opt/parser.py "${component_name}" "/opt/${component_name}.json" /mnt/config
elif [ -e "/mnt/config/init/template.jsonnet" ]; then
  # new version of operator 2.0
  cat < /mnt/config/init/template.jsonnet | base64 -d > /mnt/config/template.jsonnet
  cat < /mnt/config/init/data.json | base64 -d > /mnt/config/data.json
  /opt/config.py "${pod_name}" /mnt/config/data.json /mnt/config/template.jsonnet /mnt/config/"${component_name}".json
  cat /mnt/config/"${component_name}".json
  /opt/parser.py "${component_name}" /mnt/config/"${component_name}".json /mnt/config
  # write to both locations to support image overrides
  mkdir -p /mnt/config/pod/"${pod_name}"
  /opt/parser.py "${component_name}" /mnt/config/"${component_name}".json /mnt/config/pod/"${pod_name}"
  # clean up excess output files
  rm /mnt/config/template.jsonnet /mnt/config/data.json /mnt/config/"${component_name}".json
else
  echo "template.jsonnet not found in the path"
  exit 1
fi

# load scripts accordingly
if [ -n "${OPERATOR_CP_TYPE}" ]; then
echo "load script for CP type: ${OPERATOR_CP_TYPE}"
  if [ "${OPERATOR_CP_TYPE}" = "zookeeper" ]; then
    cp -r /opt/confluentinc/zookeeper/ /mnt/config/
  elif [ "${OPERATOR_CP_TYPE}" = "controlcenter" ]; then
    cp -r /opt/confluentinc/controlcenter/ /mnt/config/
  elif [ "${OPERATOR_CP_TYPE}" = "kafka" ]; then
    cp -r /opt/confluentinc/kafka/ /mnt/config/
  elif [ "${OPERATOR_CP_TYPE}" = "replicator" ]; then
    cp -r /opt/confluentinc/replicator/ /mnt/config/
  elif [ "${OPERATOR_CP_TYPE}" = "connect" ]; then
    cp -r /opt/confluentinc/connect/ /mnt/config/
  elif [ "${OPERATOR_CP_TYPE}" = "ksqldb" ]; then
    cp -r /opt/confluentinc/ksqldb/ /mnt/config/
  elif [ "${OPERATOR_CP_TYPE}" = "schemaregistry" ]; then
    cp -r /opt/confluentinc/schemaregistry/ /mnt/config/
  fi
fi
